data:extend({
    {
        type = "bool-setting",
        name = "extended-descriptions-fluid-colors",
        setting_type = "startup",
        default_value = true,
    },
    {
        type = "bool-setting",
        name = "extended-descriptions-voiding",
        setting_type = "startup",
        default_value = true,
    },
    {
        type = "bool-setting",
        name = "extended-descriptions-inserter-stats",
        setting_type = "startup",
        default_value = true,
    },
    {
        type = "bool-setting",
        name = "extended-descriptions-solar-ratios",
        setting_type = "startup",
        default_value = true,
    }
})

